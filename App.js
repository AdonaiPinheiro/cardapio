import React, { Component } from 'react';
import { createAppContainer, createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Home from './src/Home';
import Contato from './src/Contato';
import Horarios from './src/Horarios';
import Sobre from './src/Sobre'

const AppNavigator = createBottomTabNavigator({
  Home:{
    screen:Home,
    navigationOptions:{
      tabBarLabel:'Home',
      tabBarIcon: ({ focused, tintColor }) => {
        if(focused) {
            return(<Icon name='home' size={20} color={tintColor} />);
        } else {
            return(<Icon name='home' size={20} color={tintColor} />);
        }
      }
    }
  },
  Contato:{
    screen:Contato,
    navigationOptions:{
      tabBarLabel:'Contato',
      tabBarIcon: ({ focused, tintColor }) => {
        if(focused) {
            return(<Icon name='phone' size={20} color={tintColor} />);
        } else {
            return(<Icon name='phone' size={20} color={tintColor} />);
        }
      }
    }
  },
  Horarios:{
    screen:Horarios,
    navigationOptions:{
      tabBarLabel:'Horários',
      tabBarIcon: ({ focused, tintColor }) => {
        if(focused) {
            return(<Icon name='clock' size={20} color={tintColor} />);
        } else {
            return(<Icon name='clock' size={20} color={tintColor} />);
        }
      }
    }
  },
  Sobre:{
    screen:Sobre,
    navigationOptions:{
      tabBarLabel:'Sobre',
      tabBarIcon: ({ focused, tintColor }) => {
        if(focused) {
            return(<Icon name='address-card' size={20} color={tintColor} />);
        } else {
            return(<Icon name='address-card' size={20} color={tintColor} />);
        }
      }
    }
  }
}, {
  tabBarOptions:{
    showIcon:true,
    labelStyle:{
      fontSize:15
    },
    tabStyle:{
      justifyContent:'center',
    },
    style:{
      justifyContent:'center',
    }
  }
});

export default createAppContainer(AppNavigator);