import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';

import HomeList from './HomeList';
import HomeProducts from './HomeProducts';

const HomeNavigation = createStackNavigator({
    HomeList:{
        screen:HomeList
    },
    HomeProducts:{
        screen:HomeProducts
    }
}, {
    headerLayoutPreset:'center'
});

export default HomeNavigation;